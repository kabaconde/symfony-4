<?php

namespace App\Controller\Front;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


/**
 * @Route(name="app_front_default_")
 */
class DefaultController extends AbstractController
{
    /**
     * @Route(name="index", path="/", methods={"GET"})
     */
    public function index(): Response
    {
        return $this->render('Front/Default/index.html.twig', ['toto' => 'toto']);
    }
}
