<?php

namespace App\Controller\Back;

use App\Entity\Article;
use App\Form\ArticleType;
use App\Repository\ArticleRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route(name="app_back_article_", path="/article")
 */
class ArticleController extends AbstractController
{
    /**
     * @Route(name="index", path="/", methods={"GET"})
     */
    public function index(ArticleRepository $articleRepository): Response
    {
        // $em = $this->getDoctrine()->getManager();
        // $articles = $em->getRepository(Article::class)->findAll();
        // dd($articleRepository->findAll());
        // return $this->render('Article/index.html.twig', ['articles' => $articleRepository->findAll()]);
        return $this->render('Back/Article/index.html.twig', ['articles' => $articleRepository->findBy(['deleted' => false])]);
    }

    /**
     * @Route(name="new", path="/new", methods={"GET", "POST"})
     */
    function new (Request $request) {
        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($article);
            $em->flush();

            $this->addFlash('success', 'Article créé');

            return $this->redirectToRoute('app_back_article_index');
        }

        return $this->render('Back/Article/new.html.twig',
            [
                'article' => $article,
                'form' => $form->createView(),
            ]);
    }

    /**
     * @Route(name="show", path="/{id}", methods={"GET"})
     */
    public function show(Article $article): Response
    {
        // $em = $this->getDoctrine()->getManager();
        // $articles = $em->getRepository(Article::class)->findId($id);
        return $this->render('Back/Article/show.html.twig', ['article' => $article]);
    }

    /**
     * @Route(name="edit", path="/edit/{id}", methods={"GET", "POST"})
     */
    public function edit(Request $request, Article $article)
    {
        $form = $this->createForm(ArticleType::class, $article);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($article);
            $em->flush();

            $this->addFlash('success', 'Article modifié');

            return $this->redirectToRoute('app_back_article_show', ['id' => $article->getId() ]);
        }

        return $this->render('Back/Article/edit.html.twig',
            [
                'article' => $article,
                'form' => $form->createView(),
            ]);
    }

    /**
     * @Route(name="delete", path="/delete/{id}", methods={"GET"})
     */
    public function delete(Request $request, Article $article)
    {
        if (!$this->isCsrfTokenValid('delete-item', $request->query->get('_token'))) {
            throw new AccessDeniedException('Access Denied');
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($article);
        // $article->setDeleted(true);
        $em->flush();

        return $this->redirectToRoute('app_back_article_index');
    }
}
