<?php

namespace App\Controller\Back;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


/**
 * @Route(name="app_back_default_")
 */
class DefaultController extends AbstractController
{
    /**
     * @Route(name="index", path="/", methods={"GET"})
     */
    public function index(): Response
    {
        return $this->render('Back/Default/index.html.twig', ['toto' => 'toto']);
    }

    // /**
    //  * @Route(name="saymyname", path="/{name}", methods={"GET"})
    //  */
    // public function sayMyName($name): Response
    // {
    //     return $this->render('Default/saymyname.html.twig', ['name' => $name]);
    // }
}
